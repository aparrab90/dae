﻿using Microsoft.EntityFrameworkCore;

namespace daeapi.Models
{
    public class DemoContext : DbContext
    {
        public DemoContext(DbContextOptions<DemoContext> options) : base(options)
        {
        }
        public DbSet<Product> Products { get; set; }
    }
}
